//
//  MainViewController.swift
//  Molgenis
//
//  Created by David van Enckevort on 08-07-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet weak var server: UITextField!
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    var kbHeight: CGFloat?
    
    var molgenis: Molgenis?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        server.delegate = self
        username.delegate = self
        password.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(MainViewController.keyboardWillShow(notification:)),
                                                         name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(MainViewController.keyboardWillHide(notification:)),
                                                         name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        server.text = UserDefaults.standard.string(forKey: "server")
        username.text = UserDefaults.standard.string(forKey: "username")
    }
    
    // MARK: Notifications
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let userInfo = notification.userInfo else { return }
        guard let keyboardSize = (userInfo[UIKeyboardFrameBeginUserInfoKey]
            as? NSValue)?.cgRectValue else { return }
        if (kbHeight == nil) {
            kbHeight = keyboardSize.height
            moveView(up: true)
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if kbHeight != nil {
            moveView(up: false)
            kbHeight = nil
        }
    }
    
    func moveView(up: Bool) {
        if let height = kbHeight {
            let movement = (up ? -height : height)
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
                })
            }
        }
    }
    
    @IBAction func logout(segue: UIStoryboardSegue) {
        molgenis = nil
    }
    
    @IBAction func save(_ sender: UIButton) {
        let server = self.server.text
        let username = self.username.text
        UserDefaults.standard.set(server, forKey: "server")
        UserDefaults.standard.set(username, forKey: "username")
    }
    @IBAction func login(_ sender: UIButton) {
        guard let text = server.text, text.count > 0,
            let username = username.text, username.count > 0,
            let password = password.text, password.count > 0
        else {
            let alert = UIAlertController(title: "Login details required", message: "You must fill in all fields", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        if let url = URL(string: text), let molgenis = Molgenis(url: url) {
            self.molgenis = molgenis
            molgenis.login(username: username, password: password, completionHandler: { (_) in
                DispatchQueue.main.async {
                    self.performSegue(withIdentifier: "exploreData", sender: self)
                }
            }, errorHandler: { (error) in
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Failed to login", message: error, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            })
        } else {
            let alert = UIAlertController(title: "Failed to login", message: "An unexpected error occurred", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "exploreData" {
            let splitViewController = segue.destination as! UISplitViewController
            let navigationController = splitViewController.viewControllers[splitViewController.viewControllers.count-1] as! UINavigationController
            navigationController.topViewController!.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem
            splitViewController.delegate = self
            let masterViewController = (splitViewController.viewControllers.first as! UINavigationController).viewControllers.first as! MasterViewController
            masterViewController.molgenis = molgenis
        }        
    }
}

extension MainViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}

extension MainViewController: UISplitViewControllerDelegate {
    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController:UIViewController, onto primaryViewController:UIViewController) -> Bool {
        guard let secondaryAsNavController = secondaryViewController as? UINavigationController else { return false }
        guard let topAsDetailController = secondaryAsNavController.topViewController as? DetailViewController else { return false }
        if topAsDetailController.entity == nil {
            // Return true to indicate that we have handled the collapse by doing nothing; the secondary controller will be discarded.
            return true
        }
        return false
    }    
}
