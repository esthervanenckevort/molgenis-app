//
//  Scope.swift
//  Molgenis
//
//  Created by David van Enckevort on 17-07-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import Foundation

enum Scope: String {
    case All = "All"
    case System = "System"
    case User = "User"
    static var values: [Scope] {
        return [.User, .System, .All]
    }
}
