//
//  PopoverViewController.swift
//  Molgenis
//
//  Created by David van Enckevort on 06-07-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import UIKit

class PopoverViewController: UIViewController {
    @IBOutlet weak var detailText: UILabel!
    var label: String? {
        didSet {
            updateView()
        }
    }
    
    override func viewDidLoad() {
        updateView()
        detailText.sizeToFit()
        let frame = detailText.frame
        let size = CGSize(width: frame.width + 32, height: frame.height + 32)
        self.preferredContentSize = size
    }
    
    private func updateView() {
        detailText?.text = label
    }
}

