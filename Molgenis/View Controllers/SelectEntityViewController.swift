//
//  SelectEntityViewController.swift
//  Molgenis
//
//  Created by David van Enckevort on 09-07-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import UIKit

class SelectEntityViewController: UITableViewController {
    var molgenis: Molgenis?
    @IBOutlet weak var searchbar: UISearchBar!
    var currentEntity: String?
    var entityList: EntityList?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchbar.scopeButtonTitles = Scope.values.map() { $0.rawValue }
        entityList = EntityList(molgenis: molgenis!,
            completionHandler: { [weak self] in DispatchQueue.main.async { self?.tableView.reloadData() }},
            errorHandler: { [weak self] (error) in DispatchQueue.main.async { self?.presentError(title: "Failed to load entities", message: "Could not load a list of entities.") }})
    }
    
    private func presentError(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let entityList = entityList else { return nil }
        let package = entityList.groups[section]
        return entityList.title(package: package)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return entityList?.groups.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let key = entityList!.groups[section]
        return entityList!.grouped[key]!.sorted.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "Entity", for: indexPath)
        let package = entityList!.groups[indexPath.section]
        if let entity = entityList!.grouped[package]?.sorted[indexPath.row] {
            cell.textLabel?.text = entity.label
            cell.detailTextLabel?.text = package
            if (entity.id == currentEntity) {
                cell.accessoryType = .checkmark
            } else {
                cell.accessoryType = .none
            }
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = entityList!.groups[indexPath.section]
        if let entity = entityList!.grouped[section]?.sorted[indexPath.row] {
            currentEntity = entity.id
            performSegue(withIdentifier: "entitySelected", sender: self)
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else { return }
        switch (identifier) {
        case "entitySelected":
            let masterViewController = segue.destination as! MasterViewController
            masterViewController.entity = currentEntity
        default:
            print("SelectEntityViewController: Unhandled segue with identifier \(identifier)")
        }
    }
}

extension SelectEntityViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        updateSearch()
    }
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        updateSearch()
    }
    private func updateSearch() {
        let scope = searchbar.scopeButtonTitles![searchbar.selectedScopeButtonIndex]
        self.entityList?.scope = Scope(rawValue: scope) ?? .All
        self.entityList?.filter = searchbar.text
        self.tableView.reloadData()
    }
}
