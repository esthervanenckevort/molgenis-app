//
//  EntityList.swift
//  Molgenis
//
//  Created by David van Enckevort on 17-07-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import Foundation

class EntityList {
    private var entities: [Entity] = []
    private var packages: [Entity] = []
    var filter: String?
    var scope: Scope = .User
    var sorted: [Entity] {
        return entities.sorted()
    }
    var grouped: [String:EntityList] {
        guard let attribute = entities.first?.attribute(for: "package") else { return [:] }
        var grouped = [String:EntityList]()
        for entity in entities {
            if let filter = filter, filter.count > 0, !entity.label.lowercased().contains(filter.lowercased()) {
                continue
            }
            let package = entity[attribute: attribute] as! EntityData
            let key = package["id"] as! String
            if let list = grouped[key] {
                list.entities.append(entity)
                grouped[key] = list
            } else {
                grouped[key] = EntityList(entities: [entity], filter: filter, scope: scope)
            }
        }
        return grouped
    }
    var groups: [String] {
        return grouped.keys.filter({
            let system = $0.starts(with: "sys_") || $0 == "sys"
            switch (scope) {
            case .System:
                return system
            case .User:
                return !system
            case .All:
                return true
            }
        }).sorted()
    }

    init(entities: [Entity], filter: String? = nil, scope: Scope = .User) {
        self.entities = entities
        self.filter = filter
        self.scope = scope
    }
    
    func title(package name: String) -> String {
        return packages.filter { return ($0.id == name) }.map { return $0.label }.first ?? name
    }
    
    init(molgenis: Molgenis, completionHandler: @escaping () -> (), errorHandler: @escaping (String) -> ()) {
        let _ = molgenis.loadData(for: "sys_md_EntityType", search: nil, completionHandler: { (items, metadata, token) in
            for item in items {
                let entity = Entity(metadata: metadata, data: item)
                self.entities.append(entity)
            }
            completionHandler()
            return true
        }, errorHandler: errorHandler)
        let _ = molgenis.loadData(for: "sys_md_Package", search: nil, completionHandler: { (items, metadata, token) -> (Bool) in
            for item in items {
                self.packages.append(Entity(metadata: metadata, data: item))
            }
            return true
        }, errorHandler: { print($0)})
    }
}
