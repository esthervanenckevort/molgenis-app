//
//  NewSearchCriterionViewController.swift
//  Molgenis
//
//  Created by David van Enckevort on 25-07-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import UIKit

class NewSearchCriterionViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var subject: UITableView!
    @IBOutlet weak var predicate: UISegmentedControl!
    @IBOutlet weak var object: UITableView!
    var search: Constraint?
    var metadata: EntityType?

    override func viewDidLoad() {
        super.viewDidLoad()
    
        if search == nil {
            predicate.isHidden = true
            object.isHidden = true
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == subject {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
            return cell
        }
    }

}
