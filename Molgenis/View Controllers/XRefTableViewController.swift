//
//  XRefTableViewController.swift
//  Molgenis
//
//  Created by David van Enckevort on 15-07-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import UIKit

class XRefTableViewController: UITableViewController {
    var attribute: Attribute?
    var entity: Entity?
    var molgenis: Molgenis?
    private var sessionToken: UUID?
    private var refEntity = [Entity]()
    private var selectedEntity: Entity?
    private var cellFactory: UITableViewCellFactory?

    override func viewDidLoad() {
        super.viewDidLoad()
        cellFactory = UITableViewCellFactory(for: tableView)
        loadSelectedEntityValue()
        loadData()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return selectedEntity?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedEntity?[section].count ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let value = selectedEntity![indexPath: indexPath]
        return cellFactory!.create(cellForRowAt: indexPath, for: value)
    }
    
    private func loadSelectedEntityValue() {
        guard let molgenis = molgenis, let attribute = attribute, let item = entity?[attribute: attribute] as? EntityData else { return }
        molgenis.entity(for: item) { self.selectedEntity = $0; self.tableView.reloadData() }
    }
    
    private func loadData() {
        if let molgenis = molgenis, let attribute = attribute, let entity = attribute.refEntity?.name {
            sessionToken = molgenis.loadData(for: entity, search: nil, completionHandler: { (items, metadata, token) in
                DispatchQueue.main.sync {
                    if (token == self.sessionToken) {
                        for item in items {
                            let entity = Entity(metadata: metadata, data: item, isEditing: false)
                            self.refEntity.append(entity)
                        }
                        self.tableView.reloadData()
                    } else {
                        print("Session token \(String(describing: token)) is invalid. Discarding data.")
                    }
                }
                return token == self.sessionToken
            }, errorHandler: { (error) in
                DispatchQueue.main.async {
                    print("Error loading data \(error)")
                    let alert = UIAlertController(title: "Failed to load", message: "Could not load data from MOLGENIS.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }})
        }
    }
}
