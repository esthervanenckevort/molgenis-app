//
//  ReferenceEntryPopoverViewController.swift
//  Molgenis
//
//  Created by David van Enckevort on 26-07-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import UIKit

class ReferenceEntryPopover: UIViewController, UISearchBarDelegate, DataEntryPopover {
    var delegate: DataEntryPopoverDelegate?
    var value: Any?
    var attribute: Attribute?
    var items: [EntityData]?
    var predicate: Predicate?

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!

    override func viewDidLoad() {
        textLabel.text = attribute?.label
        descriptionLabel.text = attribute?.description
        tableView.allowsSelection = true
        let doesAllowMultiSelect = predicate == .IN || predicate == .OUT
        tableView.allowsMultipleSelection = doesAllowMultiSelect
        
        super.viewDidLoad()
    }

    @IBAction func tappedSet(_ sender: UIButton) {
        if let indexPaths = tableView.indexPathsForSelectedRows, let attribute = attribute {
            var values = [Any]()
            for indexPath in indexPaths {
                if attribute.fieldType == .ENUM {
                    values.append(attribute.enumOptions![indexPath.row])
                } else {
                    let id = attribute.refEntity!.idAttribute
                    values.append(items![indexPath.row][id]!)
                }
            }
            value = values
            delegate?.didSet(value: value!)
        }
        presentingViewController?.dismiss(animated: true, completion: nil)
    }
    @IBAction func tappedDismiss(_ sender: UIButton) {
        presentingViewController?.dismiss(animated: true, completion: nil)
    }
}

extension ReferenceEntryPopover: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let attribute = attribute else { return 0 }
        if attribute.fieldType == .ENUM {
            return attribute.enumOptions?.count ?? 0
        } else {
            guard let items = items else { return 0 }
            return items.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.selectionStyle = .blue
        if attribute?.fieldType == .ENUM {
            cell.textLabel?.text = attribute?.enumOptions?[indexPath.row]
        } else {
            guard let items = items, let label = attribute?.refEntity?.labelAttribute else { return cell }
            cell.textLabel?.text = items[indexPath.row][label] as? String
        }
        return cell
    }

//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        guard let predicate = predicate else { return }
//        let cell = tableView.cellForRow(at: indexPath)
//        cell?.accessoryType = .checkmark
//        let allowMultiselect = predicate == .IN || predicate == .OUT
//        if  !allowMultiselect {
//
//        }
//    }
}
