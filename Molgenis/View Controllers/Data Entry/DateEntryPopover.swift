//
//  dateEntryPopupController.swift
//  Molgenis
//
//  Created by David van Enckevort on 26-07-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import UIKit

class DateEntryPopover: UIViewController, DataEntryPopover {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var datePicker: UIDatePicker!

    var value: Any?
    var delegate: DataEntryPopoverDelegate?
    var attribute: Attribute?

    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = attribute?.name
        descriptionLabel.text = attribute?.description
        if let date = value as? Date {
            datePicker.setDate(date, animated: false)
        }
        if let type = attribute?.fieldType {
            if type == .DATE {
                datePicker.datePickerMode = UIDatePickerMode.date
            } else {
                datePicker.datePickerMode = UIDatePickerMode.dateAndTime
            }
        }
    }
    @IBAction func tappedSet(_ sender: UIButton) {
        delegate?.didSet(value: datePicker.date)
        presentingViewController?.dismiss(animated: true, completion: nil)
    }

    @IBAction func tappedDismiss(_ sender: UIButton) {
        presentingViewController?.dismiss(animated: true, completion: nil)
    }
}
