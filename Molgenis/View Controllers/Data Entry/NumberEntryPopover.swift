//
//  NumberEntryPopoverViewController.swift
//  Molgenis
//
//  Created by David van Enckevort on 26-07-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import UIKit

class NumberEntryPopover: UIViewController, UITextFieldDelegate, DataEntryPopover {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var numberEntryField: UITextField!
    @IBOutlet weak var numberStepper: UIStepper!

    var delegate: DataEntryPopoverDelegate?
    var attribute: Attribute?
    var value: Any?

    override func viewDidLoad() {
        super.viewDidLoad()
        if let attribute = attribute {
            descriptionLabel.text = attribute.description
            titleLabel.text = attribute.label
            numberEntryField.keyboardType = UIKeyboardType.keyboard(for: attribute.fieldType)
        }
    }

    @IBAction func tappedDismiss(_ sender: UIButton) {
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }

    private func addErrorIcon() {
        let warning = UILabel()
        warning.text = "⚠️"
        numberEntryField.rightView = warning
        numberEntryField.rightViewMode = .always
    }


    @IBAction func tappedSet(_ sender: UIButton) {
        guard let text = numberEntryField.text, let type = attribute?.fieldType else {
            return
        }
        if type == .INT || type == .LONG {
            value = Int(text) as Any?
        } else {
            value = Double(text) as Any?
        }
        if let value = value {
            self.delegate?.didSet(value: value)
            self.presentingViewController?.dismiss(animated: true, completion: nil)
        } else {
            addErrorIcon()
        }
    }
}

