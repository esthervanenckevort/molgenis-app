//
//  BooleanEntryPopoverViewController.swift
//  Molgenis
//
//  Created by David van Enckevort on 26-07-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import UIKit

class BooleanEntryPopover: UIViewController, DataEntryPopover {

    var delegate: DataEntryPopoverDelegate?
    var value: Any?
    var attribute: Attribute?

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var booleanSet: UISegmentedControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = attribute?.label
        descriptionLabel.text = attribute?.description
        if let value = value as? Bool {
            switch value {
            case true:
                booleanSet.selectedSegmentIndex = 0
            case false:
                booleanSet.selectedSegmentIndex = 1
            }
        } else {
            booleanSet.selectedSegmentIndex = 2
        }
    }

    @IBAction func tappedSet(_ sender: UIButton) {
        let index = booleanSet.selectedSegmentIndex
        switch index {
        case 0: value = true
        case 1: value = false
        default: value = nil
        }
        if let value = value {
            delegate?.didSet(value: value)
            presentingViewController?.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func tappedDismiss(_ sender: UIButton) {
        presentingViewController?.dismiss(animated: true, completion: nil)
    }
}
