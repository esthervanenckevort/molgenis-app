//
//  StringEntryPopoverViewController.swift
//  Molgenis
//
//  Created by David van Enckevort on 26-07-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import UIKit

class StringEntryPopover: UIViewController, DataEntryPopover {

    var delegate: DataEntryPopoverDelegate?
    var value: Any?
    var attribute: Attribute?

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var textField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        if let attribute = attribute {
            descriptionLabel.text = attribute.description
            titleLabel.text = attribute.label
            textField.keyboardType = UIKeyboardType.keyboard(for: attribute.fieldType)
        }
        textField.text = value as? String
    }

    @IBAction func tappedSet(_ sender: UIButton) {
        value = textField.text
        if let value = value {
            delegate?.didSet(value: value)
        }
        presentingViewController?.dismiss(animated: true, completion: nil)
    }
    @IBAction func tappedDismiss(_ sender: UIButton) {
        presentingViewController?.dismiss(animated: true, completion: nil)
    }
}
