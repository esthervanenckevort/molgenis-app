//
//  UIViewController.swift
//  Molgenis
//
//  Created by David van Enckevort on 27-07-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

protocol DataEntryPopover {
    var delegate: DataEntryPopoverDelegate? { get set }
    var value: Any? { get set }
    var attribute: Attribute? { get set }
}

protocol DataEntryPopoverDelegate {
    func didSet(value: Any)
}
