//
//  ReferenceTableViewController.swift
//  Molgenis
//
//  Created by David van Enckevort on 15-07-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import UIKit

class ReferenceTableViewController: UITableViewController {

    var attribute: Attribute?
    var entity: Entity?
    var molgenis: Molgenis?
    private var sessionToken: UUID?
    private var refEntity = [Entity]()
    private var selectedValues = [String:String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setSelectedValues()
        loadData()
        tableView.reloadData()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let attribute = attribute {
            switch (attribute.fieldType) {
            case .MREF:
                return selectedValues.count
            case .CATEGORICAL, .CATEGORICAL_MREF:
                return refEntity.count
            default:
                return 0
            }
        }
        return 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "option", for: indexPath)
        if let attribute = attribute {
            switch (attribute.fieldType) {
            case .CATEGORICAL_MREF, .CATEGORICAL:
                let cellEntity = refEntity[indexPath.row]
                cell.textLabel?.text = cellEntity[string: cellEntity.metadata.labelAttribute] as? String
                if let id = cellEntity[string: cellEntity.metadata.idAttribute] as? String, let _ = selectedValues[id] {
                    cell.accessoryType = .checkmark
                } else {
                    cell.accessoryType = .none
                }
            default:
                cell.accessoryType = .none
                let key = Array(selectedValues.keys)[indexPath.row]
                cell.textLabel?.text = selectedValues[key]
            }
        }
        return cell
    }

    private func setSelectedValues() {
        guard let attribute = attribute, let entity = entity, let refEntity = attribute.refEntity else { return }
        switch attribute.fieldType {
        case .CATEGORICAL, .XREF:
            guard let value = entity[attribute: attribute] as? EntityData else { return }
            if let id = value[refEntity.idAttribute] as? String, let label = value[refEntity.labelAttribute] as? String {
                selectedValues[id] = label
            }
        case .CATEGORICAL_MREF, .MREF:
            guard let values = entity[attribute: attribute] as? [EntityData] else { return }
            for value in values {
                if let id = value[refEntity.idAttribute] as? String, let label = value[refEntity.labelAttribute] as? String {
                    selectedValues[id] = label
                }
            }
        default:
            return
        }
    }

    private func loadData() {
        if let molgenis = molgenis, let attribute = attribute, let entity = attribute.refEntity?.name {
            sessionToken = molgenis.loadData(for: entity, search: nil, completionHandler: { (items, metadata, token) in
                DispatchQueue.main.sync {
                    if (token == self.sessionToken) {
                        for item in items {
                            let entity = Entity(metadata: metadata, data: item, isEditing: false)
                            self.refEntity.append(entity)
                        }
                        self.tableView.reloadData()
                    } else {
                        print("Session token \(String(describing: token)) is invalid. Discarding data.")
                    }
                }
                return token == self.sessionToken
            }, errorHandler: { (error) in
                DispatchQueue.main.async {
                    print("Error loading data \(error)")
                    let alert = UIAlertController(title: "Failed to load", message: "Could not load data from MOLGENIS.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }})
        }
    }
}
