//
//  SearchWizardTableViewController.swift
//  Molgenis
//
//  Created by David van Enckevort on 18-07-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import UIKit

class SearchWizardTableViewController: UITableViewController {
    var molgenis: Molgenis?
    var metadata: EntityType?
    var search: Criterion?
    var subject: Attribute?
    var predicate: Predicate?
    var object: Any?
    var attributes: [Attribute] {
        guard let metadata = metadata else { return []}
        return metadata.allAttributes.filter {
            let unsupported = FieldType.structural.contains($0.fieldType) || FieldType.data.contains($0.fieldType)
            return !unsupported && $0.visible
        }
    }
    
    @IBOutlet weak var attributeButton: UIButton!
    @IBOutlet weak var conditionButton: UIButton!
    @IBOutlet weak var valueButton: UIButton!
    @IBOutlet weak var addButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        resetButtonState()
    }

    @IBAction func tappedAdd(_ sender: Any) {
        self.insertCriterion()
        self.tableView.reloadData()
        resetButtonState()
    }

    @IBAction func tappedValue(_ sender: UIButton) {
        guard let subject = subject else { return }
        var popover: DataEntryPopover
        switch subject.fieldType {
        case FieldType.logical:
            popover = storyboard?.instantiateViewController(withIdentifier: "booleanEntryPopover") as! BooleanEntryPopover
        case FieldType.reference:
            popover = storyboard?.instantiateViewController(withIdentifier: "referenceEntryPopover") as! ReferenceEntryPopover
            (popover as! ReferenceEntryPopover).predicate = predicate
        case FieldType.temporal:
            popover = storyboard?.instantiateViewController(withIdentifier: "dateEntryPopover") as! DateEntryPopover
        case FieldType.numeric:
            popover = storyboard?.instantiateViewController(withIdentifier: "numberEntryPopover") as! NumberEntryPopover
        case FieldType.text:
            popover = storyboard?.instantiateViewController(withIdentifier: "stringEntryPopover") as! StringEntryPopover
        default:
            return
        }
        popover.attribute = subject
        popover.value = object
        popover.delegate = self
        self.present(popover: popover as! UIViewController)

    }

    private func resetButtonState() {
        self.conditionButton.isEnabled = false
        self.valueButton.isEnabled = false
        self.addButton.isEnabled = false
        self.attributeButton.setTitle("Field", for: .normal)
        self.conditionButton.setTitle("Condition", for: .normal)
        self.valueButton.setTitle("Value", for: .normal)
    }

    private func insertCriterion() {
        let criterion = Constraint(subject: self.subject!, predicate: self.predicate!, object: self.object!)
        if var search = self.search {
            while search.next != nil {
                search = search.next!
            }
            _ = search.and(criterion: criterion)
        } else {
            self.search = criterion
        }
    }

    @IBAction func tappedCondition(_ sender: UIButton) {
        let alert = UIAlertController(title: "Search condition", message: "Set the search condition.", preferredStyle: .actionSheet)
        var predicates: [Predicate]
        switch (subject!.fieldType) {
        case FieldType.logical:
            predicates = [.EQ, .NE]
        case FieldType.reference:
            predicates = [.EQ, .NE, .IN]
        case FieldType.temporal, FieldType.numeric:
            predicates = [.EQ, .NE, .GE, .GT, .LE, .LT]
        case FieldType.text:
            predicates = [.EQ, .NE, .LIKE]
        default:
            predicates = []
        }
        for predicate in predicates {
            alert.addAction(UIAlertAction(title: predicate.label, style: .default) {
                (_) in
                DispatchQueue.main.async {
                    self.predicate = predicate
                    self.conditionButton.setTitle(predicate.label, for: .normal)
                    self.valueButton.isEnabled = true
                }
            })
        }
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    @IBAction func tappedAttribute(_ sender: UIButton) {
        let alert = UIAlertController(title: "Search field", message: "Select the field to search on.", preferredStyle: .actionSheet)
        for attribute in attributes {
            alert.addAction(UIAlertAction(title: attribute.label, style: .default) {
                (_) in
                DispatchQueue.main.async {
                    self.attributeButton.setTitle(attribute.label, for: .normal)
                    self.subject = attribute
                    self.conditionButton.isEnabled = true
                }
            })
        }
        self.present(alert, animated: true, completion: nil)
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func criteriaCount(_ criterion: Criterion?) -> Int {
        guard let criterion = criterion else { return 0 }
        return 1 + criteriaCount(criterion.next)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return criteriaCount(search)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var criterion = self.criterion(at: indexPath.row)
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! FilterCriteriaCell
        let andOr = cell.accessoryView as! AndOrControl
        if let logicalOperator = criterion?.logicalOperator {
            andOr.selectedSegmentIndex = logicalOperator == .AND ? 0 : 1
            andOr.addTarget(self, action: #selector(SearchWizardTableViewController.logicalOperatorChanged(sender:forEvent:)), for: .valueChanged)
            andOr.indexPath = indexPath
        }
        cell.textLabel?.text = criterion?.string()
        return cell
    }
    
    @objc private func logicalOperatorChanged(sender: AndOrControl, forEvent event: UIEvent) {
        if let indexPath = sender.indexPath, let cell = tableView.cellForRow(at: indexPath) {
            let accessory = cell.accessoryView as! UISegmentedControl
            let option = accessory.selectedSegmentIndex
            var criterion = self.criterion(at: indexPath.row)
            criterion?.logicalOperator = option == 0 ? .AND : .OR
        }
    }
    
    private func criterion(at position: Int) -> Criterion? {
        var criterion = search
        var count = 0
        while count < position && criterion != nil {
            criterion = criterion?.next
            count += 1
        }
        return criterion
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if indexPath.row == 0 {
                search = search?.next
            } else {
                var previous = self.criterion(at: indexPath.row - 1)
                var deleted = self.criterion(at: indexPath.row)
                previous?.next = deleted?.next
            }
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else { return }
        switch (identifier) {
        case "setSearchOptions":
            let controller = segue.destination as! MasterViewController
            controller.search = search
        default:
            print("MasterViewController: Unhandled segue with identifier \(identifier)")
        }
        
    }
}

extension SearchWizardTableViewController: DataEntryPopoverDelegate {
    func didSet(value: Any) {
        self.object = value
        self.valueButton.setTitle(String(describing: value), for: .normal)
        self.addButton.isEnabled = true
    }
}
