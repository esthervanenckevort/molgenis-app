//
//  MasterViewController.swift
//  Molgenis
//
//  Created by David van Enckevort on 24-06-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController {
    
    var detailViewController: DetailViewController? = nil
    var entities: [Entity]?
    var items: [Entity]? {
        guard let entities = entities, let filter = filter, !filter.isEmpty else { return self.entities }
        return entities.filter { return $0.label.lowercased().contains(filter.lowercased()) }
    }
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchButton: UIBarButtonItem!
    var filter: String?
    var search: Criterion? {
        didSet {
            sessionToken = nil
        }
    }
    var metadata: EntityType? {
        didSet {
            searchButton?.isEnabled = metadata != nil
        }
    }
    var molgenis: Molgenis?
    // The session token is used to keep track of download fragments that belong together and should only be
    // updated on the main thread
    var sessionToken: UUID?
    var entity: String? {
        didSet {
            sessionToken = nil
            search = nil
        }
    }
    
    private func loadData() {
        entities = []
        tableView.reloadData()
        if let molgenis = molgenis, let entity = entity {
            sessionToken = molgenis.loadData(for: entity, search: search, completionHandler: { [weak self] (items, metadata, token) in
                DispatchQueue.main.sync {
                    if (token == self?.sessionToken) {
                        let entities = items.map { return Entity(metadata: metadata, data: $0)}
                        self?.entities?.append(contentsOf: entities)
                        self?.metadata = metadata
                        self?.title = metadata.label
                        self?.tableView.reloadData()
                    } else {
                        print("Session token \(String(describing: token)) is invalid. Discarding data.")
                    }
                }
                return token == self?.sessionToken
            }, errorHandler: { [weak self] (error) in
                DispatchQueue.main.async {
                    print("Error loading data \(error)")
                    self?.entity = nil
                    let alert = UIAlertController(title: "Failed to load", message: "Could not load data from MOLGENIS.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
                    self?.present(alert, animated: true, completion: nil)
                }})
        } else {
            tableView.reloadData()
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        searchBar.endEditing(true)
        searchBar.resignFirstResponder()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }
    
    @IBAction func loadEntity(segue: UIStoryboardSegue) {
        loadData()
    }
    
    @IBAction func setSearchOptions(segue: UIStoryboardSegue) {
        loadData()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else { return }
        switch (identifier) {
        case "selectEntity":
            let controller = segue.destination as! SelectEntityViewController
            controller.molgenis = self.molgenis
            controller.currentEntity = metadata?.name
            controller.modalPresentationStyle = .popover
            controller.popoverPresentationController?.delegate = self
        case "showDetail":
            if let indexPath = tableView.indexPathForSelectedRow, let items = items {
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                controller.entity = items[indexPath.row]
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
                controller.molgenis = self.molgenis
            }
        case "logout":
            break
        case "showSearchWizard":
            let controller = segue.destination as! SearchWizardTableViewController
            controller.metadata = metadata
            controller.search = search
            controller.molgenis = molgenis
        default:
            print("MasterViewController: Unhandled segue with identifier \(identifier)")
        }
    }
    
    // MARK: - Table View
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = items?[indexPath.row].label
        return cell
    }
}

extension MasterViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        updateSearch()
    }

    private func updateSearch() {
        self.filter = self.searchBar.text
        self.tableView.reloadData()
    }
}

