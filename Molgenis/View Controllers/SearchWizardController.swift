//
//  SearchWizardTableViewController.swift
//  Molgenis
//
//  Created by David van Enckevort on 18-07-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import UIKit

class SearchWizardController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
    var molgenis: Molgenis?
    var metadata: EntityType?
    var search: Criterion?
    var attributes: [Attribute] {
        guard let metadata = metadata, let attributes = metadata.attributes else { return [] }
        return asArray(attributes)
    }
    private func asArray(_ attributes: [Attribute]) -> [Attribute]{
        var list = [Attribute]()
        for attribute in attributes {
            if attribute.fieldType == .COMPOUND {
                list.append(contentsOf: asArray(attribute.attributes))
            } else {
                list.append(attribute)
            }
        }
        return list
    }
//    var subject: Attribute?
//    var predicate: Predicate?
//    var object: Any?
    
    @IBOutlet weak var tableView: UITableView!

    @IBAction func tappedTrash(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "Clear search options?", message: "Are you sure that you want to clear all search options?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Clear search options", style: .destructive) { (_) in
            DispatchQueue.main.async {
                self.search = nil
                self.tableView.reloadData()
            }
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
    }
    
    
//    @IBAction func tappedValue(_ sender: UIButton) {
//        let alert = UIAlertController(title: "Search value", message: "Select the value to search for.", preferredStyle: .alert)
//        alert.addTextField() {
//            (textField) in
//            textField.placeholder = "Type the value to search for"
//            let label = UILabel()
//            label.text = "Search term"
//            textField.leftView = label
//            textField.leftViewMode = .always
//            textField.delegate = self
//        }
//        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
//        self.present(alert, animated: true, completion: nil)
//    }
//    
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        self.object = textField.text
//        self.isLabel.isHidden = true
//        self.conditionButton.isHidden = true
//        self.valueButton.isHidden = true
//        self.attributeButton.setTitle("Attribute", for: .normal)
//        self.conditionButton.setTitle("Condition", for: .normal)
//        self.valueButton.setTitle("Value", for: .normal)
//        let criterion = Constraint(subject: self.subject!, predicate: self.predicate!, object: self.object!)
//        if var search = self.search {
//            while search.next != nil {
//                search = search.next!
//            }
//            _ = search.and(criterion: criterion)
//        } else {
//            self.search = criterion
//        }
//        self.tableView.reloadData()
//    }
//    
//    @IBAction func tappedCondition(_ sender: UIButton) {
//        let alert = UIAlertController(title: "Search condition", message: "Set the search condition.", preferredStyle: .actionSheet)
//        for predicate in Predicate.values {
//            alert.addAction(UIAlertAction(title: predicate.label, style: .default) {
//                (_) in
//                DispatchQueue.main.async {
//                    self.predicate = predicate
//                    self.conditionButton.setTitle(predicate.label, for: .normal)
//                    self.valueButton.isHidden = false
//                }
//            })
//        }
//        self.present(alert, animated: true, completion: nil)
//    }
//    @IBAction func tappedAttribute(_ sender: UIButton) {
//        let alert = UIAlertController(title: "Search field", message: "Select the field to search on.", preferredStyle: .actionSheet)
//        for attribute in attributes {
//            alert.addAction(UIAlertAction(title: attribute.label, style: .default) {
//                (_) in
//                DispatchQueue.main.async {
//                    self.attributeButton.setTitle(attribute.label, for: .normal)
//                    self.subject = attribute
//                    self.isLabel.isHidden = false
//                    self.conditionButton.isHidden = false
//                }
//            })
//        }
//        self.present(alert, animated: true, completion: nil)
//    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func criteriaCount(_ criterion: Criterion?) -> Int {
        guard let criterion = criterion else { return 0 }
        return 1 + criteriaCount(criterion.next)
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return nil
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return criteriaCount(search)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var criterion = self.criterion(at: indexPath.row)
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! FilterCriteriaCell
        let andOr = cell.accessoryView as! AndOrControl
        if let logicalOperator = criterion?.logicalOperator {
            andOr.selectedSegmentIndex = logicalOperator == .AND ? 0 : 1
            andOr.addTarget(self, action: #selector(SearchWizardController.logicalOperatorChanged(sender:forEvent:)), for: .valueChanged)
            andOr.indexPath = indexPath
        }
        cell.textLabel?.text = criterion?.string()
        return cell
    }
    
    @objc private func logicalOperatorChanged(sender: AndOrControl, forEvent event: UIEvent) {
        if let indexPath = sender.indexPath, let cell = tableView.cellForRow(at: indexPath) {
            let accessory = cell.accessoryView as! UISegmentedControl
            let option = accessory.selectedSegmentIndex
            var criterion = self.criterion(at: indexPath.row)
            criterion?.logicalOperator = option == 0 ? .AND : .OR
        }
    }
    
    private func criterion(at position: Int) -> Criterion? {
        var criterion = search
        var count = 0
        while count < position && criterion != nil {
            criterion = criterion?.next
            count += 1
        }
        return criterion
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if indexPath.row == 0 {
                search = search?.next
            } else {
                var previous = self.criterion(at: indexPath.row - 1)
                var deleted = self.criterion(at: indexPath.row)
                previous?.next = deleted?.next
            }
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else { return }
        switch (identifier) {
        case "setSearchOptions":
            let controller = segue.destination as! MasterViewController
            controller.search = search
        default:
            print("MasterViewController: Unhandled segue with identifier \(identifier)")
        }
        
    }
}
