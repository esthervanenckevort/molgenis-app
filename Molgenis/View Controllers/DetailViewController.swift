//
//  DetailViewController.swift
//  Molgenis
//
//  Created by David van Enckevort on 24-06-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import UIKit

class DetailViewController: UITableViewController {
    var entity: Entity?
    var molgenis: Molgenis?
    private var cellFactory: UITableViewCellFactory?
    
    override func viewDidLoad() {
        cellFactory = UITableViewCellFactory(for: tableView)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.rightBarButtonItem = self.editButtonItem
        tableView.reloadData()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return entity?.count ?? 0
    }
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return entity?[for: section].label
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return entity?[section].count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let value = entity![indexPath: indexPath]
        return cellFactory!.create(cellForRowAt: indexPath, for: value)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath), let (attribute, _) = entity?[indexPath: indexPath] else { return }
        switch attribute.fieldType {
        case .TEXT:
            performSegue(withIdentifier: "showPopover", sender: cell)
        case .CATEGORICAL, .CATEGORICAL_MREF, .MREF:
            performSegue(withIdentifier: "showDisclosure", sender: cell)
        case .XREF:
            performSegue(withIdentifier: "showXREF", sender: cell)
        default:
            return
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier, let entity = entity else { return }
        switch (identifier) {
        case "showDisclosure":
            guard let indexPath = tableView.indexPath(for: sender as! UITableViewCell) else {
                print("Failed to retrieve section")
                return
            }
            let (attribute, _) = entity[indexPath: indexPath]
            let vc = segue.destination as! ReferenceTableViewController
            vc.molgenis = self.molgenis
            vc.entity = self.entity
            vc.attribute = attribute
            vc.modalPresentationStyle = .popover
            vc.popoverPresentationController?.delegate = self
            vc.popoverPresentationController?.sourceRect = tableView.rectForRow(at: indexPath)
        case "showPopover":
            guard let indexPath = tableView.indexPath(for: sender as! UITableViewCell) else {
                    print("Failed to retrieve section")
                    return
            }
            let (attribute, value) = entity[indexPath: indexPath]
            let vc = segue.destination as! PopoverViewController
            vc.title = attribute.label
            vc.label = value as? String
            vc.modalPresentationStyle = .popover
            vc.popoverPresentationController?.delegate = self
            vc.popoverPresentationController?.sourceRect = tableView.rectForRow(at: indexPath)
        case "showXREF":
            guard let indexPath = tableView.indexPath(for: sender as! UITableViewCell) else {
                print("Failed to retrieve section")
                return
            }
            let (attribute, _) = entity[indexPath: indexPath]
            let vc = segue.destination as! XRefTableViewController
            vc.title = attribute.label
            vc.attribute = attribute
            vc.molgenis = molgenis
            vc.entity = entity
            vc.modalPresentationStyle = .popover
            vc.popoverPresentationController?.delegate = self
            vc.popoverPresentationController?.sourceRect = tableView.rectForRow(at: indexPath)
        default:
            print("DetailViewController: Unhandled segue with identifier \(identifier)")
        }
    }
}
