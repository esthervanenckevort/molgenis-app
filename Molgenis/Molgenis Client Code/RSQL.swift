//
//  RSQL.swift
//  Molgenis
//
//  Created by David van Enckevort on 17-07-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import Foundation

enum Predicate: String {
    case EQ = "=="
    case NE = "!="
    case LT = "=lt="
    case GT = "=gt="
    case LE = "=le="
    case GE = "=ge="
    case IN = "=in="
    case OUT = "=out="
    case LIKE = "=q="
    static let values: [Predicate] = [.EQ, .NE, .LT, .GT, .LE, .GE, .IN, .OUT, .LIKE]
    var label: String {
        switch self {
        case .EQ:
            return "="
        case .NE:
            return "≠"
        case .LT:
            return "<"
        case .GT:
            return ">"
        case .LE:
            return "≤"
        case .GE:
            return "≥"
        case .IN:
            return "one of"
        case .OUT:
            return "not one of"
        case .LIKE:
            return "like"
        }
    }
}

class Constraint: Criterion {
    var logicalOperator: LogicalOperator?
    var next: Criterion?
    var subject: Attribute
    var predicate: Predicate
    var object: Any
    private var stringRepresentation: String

    init(subject: Attribute, predicate: Predicate, object: Any) {
        self.subject = subject
        self.predicate = predicate
        self.object = object
        switch predicate {
        case .IN, .OUT:
            let items = object as! [Any]
            var strings = [String]()
            for item in items {
                strings.append(subject.toString(item))
            }
            stringRepresentation = "(\(strings.joined(separator: ",")))"
        default:
            stringRepresentation = subject.toString(object)
        }
    }

    func and(criterion: Criterion) -> Criterion {
        next = criterion
        logicalOperator = .AND
        return self
    }
    
    func or(criterion: Criterion) -> Criterion {
        next = criterion
        logicalOperator = .OR
        return self
    }

    func rsql() throws -> String {
        var rsql: String
        rsql = "\(subject.name)\(predicate.rawValue)\(stringRepresentation)"
        if let criterion = next, let op = logicalOperator {
            rsql = "\(rsql)\(op.rawValue)\(try criterion.rsql())"
        }
        return rsql
    }
    
    func type() -> CriterionType {
        return .CONSTRAINT
    }
    
    func string() -> String {
        return "\(subject.name) \(predicate.label) \(object)"
    }
}

class Group: Criterion {
    var next: Criterion?
    var logicalOperator: LogicalOperator?
    var members: Criterion

    init(members: Criterion) {
        self.members = members
    }
    
    func and(criterion: Criterion) -> Criterion {
        next = criterion
        logicalOperator = .AND
        return self
    }
    
    func or(criterion: Criterion) -> Criterion {
        next = criterion
        logicalOperator = .OR
        return self
    }
    
    func rsql() throws -> String {
        var rsql: String
        rsql = "(\(try members.rsql()))"
        if let criterion = next, let op = logicalOperator {
            rsql = "\(rsql)\(op.rawValue)\(try criterion.rsql())"
        }
        return rsql
    }
    
    func type() -> CriterionType {
        return .GROUP
    }
    
    func string() -> String {
        var string = ""
        var criterion: Criterion? = members
        while criterion != nil {
            string += criterion!.string()
            if criterion?.next != nil && criterion?.logicalOperator != nil {
                string += " \(criterion!.logicalOperator!.label )"
            }
            criterion = criterion?.next
        }
        return "(\(string)"
    }
}

enum LogicalOperator: String {
    case AND = ";"
    case OR = ","
    var label: String {
        switch self {
        case .AND:
            return "and"
        case .OR:
            return "or"
        }
    }
}

protocol Criterion {
    var logicalOperator: LogicalOperator? { get set }
    var next: Criterion? { get set }
    func and(criterion: Criterion) -> Criterion
    func or(criterion: Criterion) -> Criterion
    func rsql() throws -> String
    func type() -> CriterionType
    func string() -> String
}

enum CriterionException: Error {
    case SyntaxError
}

enum CriterionType {
    case GROUP, CONSTRAINT
}

fileprivate extension Attribute {
    func toString(_ value: Any) -> String {
        var string: String
        switch fieldType {
        case .CATEGORICAL, .CATEGORICAL_MREF, .MREF, .ONE_TO_MANY, .XREF:
            let refAttributes = refEntity!.attributes
            let id = refEntity!.idAttribute
            let idAttribute = refAttributes!.first(where: { $0.name == id })!
            string = idAttribute.toString(value)
        case .DATE:
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "yyyy-MM-dd"
            formatter.timeZone = TimeZone(secondsFromGMT: 0)
            let date = value as! Date
            string =  formatter.string(from: date)
        case .DATE_TIME:
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-ddTHH:mm:ssZZZZZ"
            formatter.timeZone = TimeZone(secondsFromGMT: 0)
            let date = value as! Date
            string =  formatter.string(from: date)
        case .DECIMAL:
            let decimal = value as! Double
            string =  "\(decimal)"
        case .INT, .LONG:
            let int = value as! Int
            string =  "\(int)"
        case .BOOL:
            let bool = value as! Bool
            string = "\(bool)"
        default:
            string =  value as! String
        }
        return string.contains(" ") ? "'\(string)'" : string
    }
}
