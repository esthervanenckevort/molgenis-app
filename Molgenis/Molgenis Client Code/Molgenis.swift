//
//  Molgenis.swift
//  Molgenis
//
//  Created by David van Enckevort on 08-07-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import Foundation

class Molgenis {
    var server: URL
    var token: String?
    
    init?(url: URL) {
        server = url
        guard var loginURL = URLComponents(url: url, resolvingAgainstBaseURL: true) else { return nil }
        loginURL.path = "/api/v2/version"
        let (data, _, _) = URLSession.shared.synchronousDataTaskWithURL(url: loginURL.url!)
        if let data = data {
            let decoder = JSONDecoder()
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "yyyy-MM-dd HH:mm ZZZZZ"
            formatter.timeZone = TimeZone(secondsFromGMT: 0)
            decoder.dateDecodingStrategy = .formatted(formatter)
            do {
                let molgenisVersion = try decoder.decode(MolgenisVersion.self, from: data)
                let (major, minor, _, _) = molgenisVersion.parseVesion()
                guard major > 1 || (major == 1 && minor >= 15) else { return nil }
            } catch {
                return nil
            }
        } else {
            return nil
        }
    }
    
    func login(username: String, password: String, completionHandler handler: ((LoginResult) -> ())?, errorHandler: ((String) -> ())?) {
        guard var url = URLComponents(url: server, resolvingAgainstBaseURL: true) else {
            errorHandler?("Failed to compose URL")
            return
        }
        url.path = "/api/v1/login"
        var request = URLRequest(url: url.url!)
        let encoder = JSONEncoder()
        request.httpBody = try! encoder.encode(LoginRequest(username: username, password: password))
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            if let data = data {
                let decoder = JSONDecoder()
                do {
                    let loginResult = try decoder.decode(LoginResult.self, from: data)
                    if loginResult.token.count > 0 {
                        self.token = loginResult.token
                    }
                    handler?(loginResult)
                } catch {
                   errorHandler?("Failed to login. Failed to parse server response.")
                }
            } else {
                errorHandler?("Failed to login. No data from server.")
            }
        }.resume()
    }
    
    func entity(for value: EntityData, completionHandler handler: @escaping (Entity) -> ()) {
        guard var url = URLComponents(url: server, resolvingAgainstBaseURL: true) else { return }
        guard let path = value["_href"] as? String else { return }
        url.path = path
        var request = URLRequest(url: url.url!)
        if let token = token {
            request.addValue(token, forHTTPHeaderField: "x-molgenis-token")
        }
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            do {
                let requestAsJSON = try JSONSerialization.jsonObject(with: data!, options: [])
                if let entityData = requestAsJSON as? EntityData {
                    let meta = try JSONSerialization.data(withJSONObject: entityData["_meta"]!, options: [])
                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .iso8601
                    let entityType = try decoder.decode(EntityType.self, from: meta)
                    let entity = Entity(metadata: entityType, data: entityData, isEditing: false)
                    DispatchQueue.main.async {
                        handler(entity)
                    }
                }
            } catch let error {
                print("Error decoding entity: \(error)")
            }
        }
        task.resume()
    }
    
    func loadData(for entity: String, search criteria: Criterion?,
                  completionHandler handler: @escaping ([EntityData], EntityType, UUID) -> (Bool),
                  errorHandler: @escaping (String) -> ()) -> UUID? {
        
        guard var url = URLComponents(url: server, resolvingAgainstBaseURL: true) else {
            errorHandler("Failed to compose URL")
            return nil
        }
        url.path = "/api/v2/\(entity)"
        if let criteria = criteria, let rsql = try? criteria.rsql() {
            let query = URLQueryItem(name: "q", value: rsql)
            url.queryItems = [query]
        }
        let sessionToken = UUID()
        print("Loading data from \(url.url!)")
        loadData(for: url.url!, sessionToken: sessionToken, completionHandler: handler, errorHandler: errorHandler)
        return sessionToken
    }
    
    private func loadData(for url: URL, sessionToken: UUID,
                          completionHandler handler: @escaping ([EntityData], EntityType, UUID) -> (Bool),
                          errorHandler: @escaping (String) -> ()) {
        
        var request = URLRequest(url: url)
        if let token = token {
            request.addValue(token, forHTTPHeaderField: "x-molgenis-token")
        }
        URLSession.shared.dataTask(with: request) { [weak self] data, response, error in
            do {
                guard let data = data else { throw MolgenisError.NoData }
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .iso8601
                var request: EntityContainer
                request = try decoder.decode(EntityContainer.self, from: data)
                var items = [EntityData]()
                do {
                    let requestAsJSON = try JSONSerialization.jsonObject(with: data, options: [])
                    if let requestAsDictionary = requestAsJSON as? EntityData,
                       let collections = requestAsDictionary["items"] as? [EntityData] {
                            items.append(contentsOf: collections)
                    } else {
                        throw MolgenisError.DecodingError
                    }
                }
                let shouldContinue = handler(items, request.meta, sessionToken)
                if shouldContinue == true, let url = request.nextHref {
                    let _ = self?.loadData(for: url, sessionToken: sessionToken, completionHandler: handler, errorHandler: errorHandler)
                }
            } catch let error {
                print("Error loading data \(error)\n")
                errorHandler("Failed to load entity data.")
            }
        }.resume()
    }
    
    enum MolgenisError: Error {
        case InitialisationError
        case DecodingError
        case NoData
        case LoginFailed
    }
}

extension URLSession {
    func synchronousDataTaskWithURL(url: URL) -> (Data?, URLResponse?, Error?) {
        var data: Data?, response: URLResponse?, error: Error?
        
        let semaphore = DispatchSemaphore(value: 0)
        
        dataTask(with: url) {
            data = $0; response = $1; error = $2
            semaphore.signal()
            }.resume()
        
        semaphore.wait()
        
        return (data, response, error)
    }
}
