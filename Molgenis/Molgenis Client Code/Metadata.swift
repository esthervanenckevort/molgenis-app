//
//  Entity.swift
//  Molgenis
//
//  Created by David van Enckevort on 24-06-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import Foundation

typealias EntityData = [String:Any]

struct Entity: Comparable {
    typealias Section = Attribute
    var metadata: EntityType
    private var data: EntityData
    var isEditing = false

    init(metadata: EntityType, data: EntityData, isEditing: Bool = false) {
        self.metadata = metadata
        self.data = data
        self.isEditing = isEditing
    }
    
    static func <(lhs: Entity, rhs: Entity) -> Bool {
        guard lhs.metadata.href == rhs.metadata.href else { return false }
        return lhs.label < rhs.label
    }
    
    static func ==(lhs: Entity, rhs: Entity) -> Bool {
        guard lhs.metadata.href == rhs.metadata.href else { return false }
        return lhs.label == rhs.label
    }
    
    private var rootSection: Attribute {
        return Attribute(href: "", fieldType: .COMPOUND, name: "_", label: nil, description: nil, attributes: (metadata.attributes?.filter() { $0.fieldType != .COMPOUND })!, refEntity: nil, maxLength: nil, auto: nil, nillable: false, readOnly: true, labelAttribute: false, unique: false, visible: false, lookupAttribute: false, isAggregatable: false, enumOptions: nil, rangeMin: nil, rangeMax: nil)
    }
    
    private var sections: [Attribute] {
        var sections = [Attribute]()
        sections.append(rootSection)
        sections.append(contentsOf: compounds(in: metadata.attributes))
        return sections
    }

    var count: Int {
        return sections.count
    }
    
    subscript(for section: Int) -> Section {
        return sections[section]
    }
    
    subscript(section: Int) -> [Attribute] {
        return sections[section].attributes.filter() { including(attribute: $0) }
    }
    
    subscript(indexPath indexPath: IndexPath) -> (attribute: Attribute, value: Any?) {
        let attribute = self[indexPath.section][indexPath.row]
        let value = self[attribute: attribute]
        return (attribute, value)
    }
    
    subscript(attribute attribute: Attribute) -> Any? {
        return data[attribute.name]
    }
    
    subscript(string name: String) -> Any? {
        return data[name]
    }
    
    var id: String {
        return self[string: metadata.idAttribute] as! String
    }
    
    var label: String {
        return self[string: metadata.labelAttribute] as! String
    }
  
    func attribute(for name: String) -> Attribute? {
        guard let attributes = metadata.attributes else { return nil }
        return attribute(for: name, in: attributes)
    }
    
    private func attribute(for name: String, in attributes: [Attribute]) -> Attribute? {
        for attribute in attributes {
            if attribute.name == name {
                return attribute
            }
            if attribute.fieldType == .COMPOUND {
                if let found = self.attribute(for: name, in: attribute.attributes) {
                    return found
                }
            }
        }
        return nil
    }

    func string(attribute: Attribute) -> String? {
        guard let value = self[attribute: attribute] else { return nil }
        switch (attribute.fieldType) {
        case .BOOL:
            return "\(value as! Bool)"
        case .CATEGORICAL, .XREF:
            let entityData = value as! EntityData
            return entityData[attribute.refEntity!.labelAttribute] as? String
        case .CATEGORICAL_MREF, .MREF, .ONE_TO_MANY:
            let items = value as! [EntityData]
            var labels = [String]()
            for entityData in items {
                let label = entityData[attribute.refEntity!.labelAttribute] as! String
                labels.append(label)
            }
            return labels.joined(separator: ", ")
        case .COMPOUND:
            return nil
        case .DATE, .DATE_TIME, .EMAIL, .ENUM, .HTML, .HYPERLINK, .SCRIPT, .STRING, .TEXT:
            return value as? String
        case .DECIMAL:
            return "\(value as! Double)"
        case .FILE:
            return String(describing: value)
        case .INT, .LONG:
            return "\(value as! Int)"
        }
    }
    
    private func compounds(in attributes: [Attribute]?) -> [Attribute] {
        guard let attributes = attributes else { return [] }
        var sections = attributes.filter() { including(section: $0) }
        var subsections = [Attribute]()
        for section in sections {
            subsections.append(contentsOf: compounds(in: section.attributes))
        }
        sections.append(contentsOf: subsections)
        return sections
    }
    
    private func including(section: Attribute) -> Bool {
        guard section.fieldType == .COMPOUND else { return false }
        guard isEditing == false else { return true }
        
        return section.attributes.filter() { data[$0.name] != nil}.count > 0
    }
    
    private func including(attribute: Attribute) -> Bool {
        guard attribute.fieldType != .COMPOUND else { return false }
        guard isEditing == false else { return true }
        switch (attribute.fieldType) {
        case .CATEGORICAL_MREF, .MREF, .ONE_TO_MANY:
            if let value = data[attribute.name] as? [Any] {
                return value.count != 0
            }
            return false
        default:
            return data[attribute.name] != nil
        }
    }
}

struct LoginRequest: Encodable {
    var username: String
    var password: String
}

struct LoginResult: Decodable {
    var token: String
    var username: String
    var firstname: String?
    var lastname: String?
}

struct MolgenisVersion: Decodable, Comparable {
    var molgenisVersion: String
    var buildDate: Date
    
    static func <(lhs: MolgenisVersion, rhs: MolgenisVersion) -> Bool {
        let (lhsMajor, lhsMinor, lhsRevision, _) = lhs.parseVesion()
        let (rhsMajor, rhsMinor, rhsRevision, _) = rhs.parseVesion()
        if rhsMajor > lhsMajor {
            return true
        }
        if rhsMajor == lhsMajor && rhsMinor > lhsMinor {
            return true
        }
        if lhsMajor == lhsMajor && rhsMinor == rhsMinor && rhsRevision > lhsRevision {
            return true
        }
        return false
    }
    
    static func ==(lhs: MolgenisVersion, rhs: MolgenisVersion) -> Bool {
        let (lhsMajor, lhsMinor, lhsRevision, lhsModifier) = lhs.parseVesion()
        let (rhsMajor, rhsMinor, rhsRevision, rhsModifier) = rhs.parseVesion()
        return lhsMajor == rhsMajor && lhsMinor == rhsMinor && lhsRevision == rhsRevision && lhsModifier == rhsModifier
    }
    
    func parseVesion() -> (major: Int, minor: Int, Revsion: Int, modifier: String?) {
        let components = molgenisVersion.components(separatedBy: ".")
        let major = Int(components[0]);
        let minor = Int(components[1]);
        if components.count == 3 {
            let parts = components[2].components(separatedBy: "-")
            var modifier: String?
            if parts.count == 2 {
                modifier = parts[1]
            }
            let revision = Int(parts[0]);
            return (major ?? 0, minor ?? 0, revision ?? 0, modifier);
        }
        return (major ?? 0, minor ?? 0, 0, nil)
    }
    var major: Int {
        get {
            let (major, _, _, _) = parseVesion()
            return major
        }
    }
    var minor: Int {
        get {
            let (_, minor, _, _) = parseVesion()
            return minor
        }
    }
    var revision: Int {
        get {
            let (_, _, revision, _) = parseVesion()
            return revision
        }
    }
    var modifier: String? {
        get {
            let (_, _, _, modifier) = parseVesion()
            return modifier
        }
    }
    
}

struct EntityContainer: Codable {
    var meta: EntityType
    var start: Int
    var num: Int
    var total: Int
    var nextHref: URL?
}

struct EntityType: Codable {
    var href: String
    var hrefCollection: String
    var name: String
    var label: String
    var description: String?
    var attributes: [Attribute]?
    var labelAttribute: String
    var idAttribute: String
    var lookupAttributes: [String]
    var isAbstract: Bool
    var writable: Bool
    var languageCode: String
    var allAttributes: [Attribute] {
        return flattened(attributes)
    }
    private func flattened(_ attributes: [Attribute]?) -> [Attribute]{
        guard let attributes = attributes else { return [] }
        var list = [Attribute]()
        for attribute in attributes {
            list.append(attribute)
            if attribute.fieldType == .COMPOUND {
                list.append(contentsOf: flattened(attribute.attributes))
            }
        }
        return list
    }}

struct Attribute: Codable {
    var href: String
    var fieldType: FieldType
    var name: String
    var label: String?
    var description: String?
    var attributes: [Attribute]
    var refEntity: EntityType?
    var maxLength: Int?
    var auto: Bool?
    var nillable: Bool
    var readOnly: Bool
    var labelAttribute: Bool
    var unique: Bool
    var visible: Bool
    var lookupAttribute: Bool
    var isAggregatable: Bool
    var enumOptions: [String]?
    var rangeMin: Int?
    var rangeMax: Int?
}

enum FieldType: String, Codable {
    case BOOL, CATEGORICAL, CATEGORICAL_MREF, COMPOUND, DATE, DATE_TIME, DECIMAL, EMAIL, ENUM, FILE, HTML, HYPERLINK, INT, LONG, MREF, ONE_TO_MANY, SCRIPT, STRING, TEXT, XREF
    static let values = [BOOL, CATEGORICAL, CATEGORICAL_MREF, COMPOUND, DATE, DATE_TIME, DECIMAL, EMAIL, ENUM, FILE, HTML, HYPERLINK, INT, LONG, MREF, ONE_TO_MANY, SCRIPT, STRING, TEXT, XREF]

    static let text = [TEXT, STRING, EMAIL, HYPERLINK, HTML]
    static let numeric = [INT, LONG, DECIMAL]
    static let temporal = [DATE, DATE_TIME]
    static let reference = [CATEGORICAL, CATEGORICAL_MREF, ONE_TO_MANY, MREF, XREF, ENUM]
    static let data = [SCRIPT, FILE]
    static let structural = [COMPOUND]
    static let logical = [BOOL]
    /*
     Override for pattern matching to allow switch statements on the above defined groups
     */
    static func ~=(pattern: [FieldType], value: FieldType) -> Bool {
        return pattern.contains(value)
    }
}
