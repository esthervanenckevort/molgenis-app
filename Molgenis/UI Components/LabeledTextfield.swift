//
//  LabeledTextfield.swift
//  Molgenis
//
//  Created by David van Enckevort on 25-07-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import UIKit

@IBDesignable class LabeledTextfield: UITextField {
    @IBInspectable var insetWidth: CGFloat = 0.0 {
        didSet {
            insertLabelView()
        }
    }
    @IBInspectable var labelOffset: CGFloat = 0.0 {
        didSet {
            insertLabelView()
        }
    }
    @IBInspectable var labelText: String? = "" {
        didSet {
            insertLabelView()
        }
    }
    @IBInspectable var labelFontPointSize: CGFloat = 10.0 {
        didSet {
            insertLabelView()
        }
    }
    
    private func insertLabelView() {
        let label = UILabel()
        if let font = self.font {
            label.font = UIFont(name: font.fontName, size: labelFontPointSize)
        }
        label.text = labelText
        label.sizeToFit()
        label.frame = CGRect(x: labelOffset, y: labelOffset, width: label.intrinsicContentSize.width, height: label.intrinsicContentSize.height)
        let viewFrame = CGRect(x: CGFloat(0.0), y: CGFloat(0.0), width: insetWidth, height: intrinsicContentSize.height)
        let view = UIView(frame: viewFrame)
        view.addSubview(label)
        self.leftView = view
        self.leftViewMode = .always
    }
}
