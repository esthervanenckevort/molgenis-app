//
//  FilterCriteriaCell.swift
//  Molgenis
//
//  Created by David van Enckevort on 24-07-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import UIKit

@IBDesignable class FilterCriteriaCell: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        accessoryView = AndOrControl(items: ["And", "Or"])
    }
}
