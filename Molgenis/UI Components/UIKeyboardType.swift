//
//  FieldType.swift
//  Molgenis
//
//  Created by David van Enckevort on 26-07-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import UIKit

extension UIKeyboardType {
    static func keyboard(for type: FieldType) -> UIKeyboardType {
        switch type {
        case .DECIMAL:
            return UIKeyboardType.decimalPad
        case .EMAIL:
            return UIKeyboardType.emailAddress
        case .HYPERLINK:
            return UIKeyboardType.URL
        case .INT, .LONG:
            return UIKeyboardType.numberPad
        default:
            return UIKeyboardType.default
        }
    }
}
