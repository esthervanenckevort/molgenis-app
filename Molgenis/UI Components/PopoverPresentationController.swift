//
//  Popover.swift
//  Molgenis
//
//  Created by David van Enckevort on 27-07-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import UIKit

class PopoverPresentationController: UIPresentationController {
    var visualEffect: UIVisualEffectView
    var size: CGSize

    override init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?) {
        visualEffect = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
        visualEffect.backgroundColor = UIColor.black
        visualEffect.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        presentedViewController.view.layer.cornerRadius = 15.0
        size = presentedViewController.minimumSize
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
    }

    override func size(forChildContentContainer container: UIContentContainer, withParentContainerSize parentSize: CGSize) -> CGSize {
        return size
    }

    override func adaptivePresentationStyle(for traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none

    }
    override var frameOfPresentedViewInContainerView: CGRect {
        let presentingSize = presentingViewController.view.frame.size
        let origin = CGPoint(x: (presentingSize.width - size.width)/2, y: presentingSize.height - size.height)
        return CGRect(origin: origin, size: size)
    }

    override var shouldPresentInFullscreen: Bool {
        return false
    }

    override func containerViewWillLayoutSubviews() {
        presentedView?.frame = frameOfPresentedViewInContainerView
    }
    override func presentationTransitionWillBegin() {
        visualEffect.frame = containerView!.bounds
        visualEffect.alpha = 0.0
        containerView!.insertSubview(visualEffect, at: 0)
        presentedViewController.transitionCoordinator?.animate(alongsideTransition: { (coordinatorContext) -> Void in
            self.visualEffect.alpha = 0.2
        }, completion: nil)
    }

    override func dismissalTransitionWillBegin() {
        presentedViewController.transitionCoordinator?.animate(alongsideTransition: { (coordinatorContext) -> Void in
            self.visualEffect.alpha = 0.0
        }, completion: { (_) in self.visualEffect.removeFromSuperview() })

    }
}
