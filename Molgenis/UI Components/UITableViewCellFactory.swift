//
//  CellRenderer.swift
//  Molgenis
//
//  Created by David van Enckevort on 15-07-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import Foundation
import UIKit

class UITableViewCellFactory {
    private let tableView: UITableView
    init(for tableView: UITableView) {
        for fieldType in FieldType.values {
            if fieldType == .EMAIL || fieldType == .HYPERLINK {
                tableView.register(UINib(nibName: "HyperlinkCell", bundle: nil), forCellReuseIdentifier: fieldType.rawValue)
            } else {
                tableView.register(UINib(nibName: "DefaultCell", bundle: nil), forCellReuseIdentifier: fieldType.rawValue)
            }
        }
        self.tableView = tableView
    }
    
    func create(cellForRowAt indexPath: IndexPath, for value: (Attribute, Any?)) -> UITableViewCell {
        let (attribute, value) = value
        let cell = tableView.dequeueReusableCell(withIdentifier: attribute.fieldType.rawValue, for: indexPath)

        switch attribute.fieldType {
        case .HYPERLINK, .EMAIL:
            guard let hyperlinkCell = cell as? HyperlinkCell else { return cell }
            if let details = value as? String {
                var url: URL?
                if attribute.fieldType == .EMAIL {
                    url = URL(string: "mailto:\(details)")
                } else {
                    url = URL(string: details)
                }
                hyperlinkCell.url = url
                hyperlinkCell.label.text = attribute.label
                hyperlinkCell.hyperlink.setTitle(details, for: .normal)
            }
        case .CATEGORICAL, .XREF:
            cell.accessoryType = .disclosureIndicator
            if let item = value, let dict = item as? [String:Any] {
                cell.detailTextLabel?.text = dict[(attribute.refEntity?.labelAttribute)!] as? String
            }
            cell.textLabel?.text = attribute.label
        case .CATEGORICAL_MREF, .MREF, .ONE_TO_MANY:
            cell.accessoryType = .disclosureIndicator
            if let item = value, let array = item as? [[String:Any]] {
                cell.detailTextLabel?.text = array.map(){
                    let dict = $0 as [String:Any]
                    return dict[(attribute.refEntity?.labelAttribute)!] as! String
                    }.joined(separator: ", ")
            }
            cell.textLabel?.text = attribute.label
        case .BOOL:
            if let bool = value as? Bool {
                cell.detailTextLabel?.text = bool ? "yes" : "no"
            } else {
                cell.detailTextLabel?.text = "unknown"
            }
            cell.textLabel?.text = attribute.label
            cell.accessoryType = .none
        case .INT, .LONG, .DECIMAL:
            if let value = value as? Int {
                cell.detailTextLabel?.text = "\(value)"
            }
            cell.textLabel?.text = attribute.label
            cell.accessoryType = .none
        case .TEXT, .STRING, .SCRIPT, .ENUM, .HTML:
            cell.detailTextLabel?.text = value as? String
            if cell.detailTextLabel?.text != nil {
                cell.accessoryType = .detailButton
            } else {
                cell.accessoryType = .none
            }
            cell.textLabel?.text = attribute.label
        case .COMPOUND:
            cell.textLabel?.text = attribute.label
            cell.accessoryType = .disclosureIndicator
        case .DATE:
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "yyyy-MM-dd"
            let date = formatter.date(from: value as! String)!
            formatter.dateStyle = .short
            formatter.timeStyle = .none
            cell.detailTextLabel?.text = formatter.string(from: date)
            cell.textLabel?.text = attribute.label
            cell.accessoryType = .none
        case .DATE_TIME:
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
            let date = formatter.date(from: value as! String)!
            formatter.dateStyle = .short
            formatter.timeStyle = .short
            cell.detailTextLabel?.text = formatter.string(from: date)
            cell.textLabel?.text = attribute.label
            cell.accessoryType = .none
        case .FILE:
            let data = value as? Data
            cell.detailTextLabel?.text = String(describing: data)
            cell.textLabel?.text = attribute.label
            cell.accessoryType = .none
        }
        return cell
    }
}

