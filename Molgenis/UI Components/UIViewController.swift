//
//  UIViewController.swift
//  Molgenis
//
//  Created by David van Enckevort on 28-07-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import UIKit

extension UIViewController: UIAdaptivePresentationControllerDelegate, UIViewControllerTransitioningDelegate, UIPopoverPresentationControllerDelegate {

    var minimumSize: CGSize {
        let maxX = view.subviews.reduce(0) { (result, view) in return result > view.frame.maxX ? result : view.frame.maxX        }
        let maxY = view.subviews.reduce(0) { (result, view) in return result > view.frame.maxY ? result : view.frame.maxY }
        return CGSize(width: maxX, height: maxY)
    }

    var minimumRect: CGRect {
        let origin = CGPoint(x: 0, y: 0)
        return CGRect(origin: origin, size: minimumSize)
    }

    func present(popover: UIViewController) {
        popover.modalPresentationStyle = .custom
        popover.preferredContentSize = popover.minimumSize
        popover.presentationController?.delegate = self
        popover.transitioningDelegate = self
        self.present(popover, animated: true, completion: nil)
    }

    public func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }

//    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        return UIViewControllerAnimatedTransitioning() {
//
//        }
//    }
    public func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return PopoverPresentationController(presentedViewController: presented, presenting: presenting)
    }
}
