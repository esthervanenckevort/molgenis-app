//
//  HyperlinkCell.swift
//  Molgenis
//
//  Created by David van Enckevort on 12-07-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import UIKit

class HyperlinkCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var hyperlink: UIButton!
    var url: URL?

    @IBAction func linkButtonTapped(_ sender: UIButton) {
        if let url = url {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
}
